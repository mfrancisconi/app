import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from "@angular/forms";
import { Router } from '@angular/router';
import { AuthService } from '../shared/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  signinForm: FormGroup;
  currentUser = {};

  constructor(
    public fb: FormBuilder,
    public authService: AuthService,
    public router: Router
  ) { 
    this.signinForm = this.fb.group({
      email: [''],
      password: ['']
    })
  }

  ngOnInit() {
  }

  loginUser() {
    this.authService.signIn(this.signinForm.value).subscribe((res: any) => {
      localStorage.setItem("access_token", res.token);

      this.authService.getUserProfile(res.id).subscribe((res) => {
        this.currentUser = res;

        this.router.navigate(["user-profile/" + res.id]);
      });
    }),
      (    err: any) => console.log('HTTP Error', err);
  }

}
