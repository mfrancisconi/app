import { Component, OnInit } from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import { AuthService } from "../shared/auth.service";
import { User } from "../shared/user";
import { ConfirmedValidator } from "../validators/confirmed.validator";

@Component({
  selector: "app-registration",
  templateUrl: "./registration.page.html",
  styleUrls: ["./registration.page.scss"],
})
export class RegistrationPage implements OnInit {
  user: User;
  registerForm: FormGroup;

  constructor(private fb: FormBuilder, private authService : AuthService) {}

  ngOnInit() {
    this.registerForm = this.fb.group({
      apellido: ['Francisconi', Validators.required],
      nombre: ['Martín', Validators.required],
      password: ['123456', [Validators.required, Validators.minLength(6)]],
      confirmPassword: ['123456', Validators.required],
      email: ['eltinchy@hotmail.com', [Validators.required, Validators.email]],
      dni: ['31552320', Validators.required],
    }, {
      validator: ConfirmedValidator('password', 'confirmPassword'),
    });
  }

  onSubmit() {
    // TODO: Use EventEmitter with form value
    this.user = this.registerForm.value;
    
    this.authService.signUp(this.user).subscribe(res => {
      console.log(res);
    });
  }
}
