export class User {
    public idUsuarioApp: number;
    public dni: number;   
    public apellido: string;
    public nombre: string;
    public password: string;
    public email: string;
}