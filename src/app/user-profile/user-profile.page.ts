import { ɵNullViewportScroller } from '@angular/common';
import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { AuthService } from "../shared/auth.service";
import { User } from "../shared/user";

@Component({
  selector: "app-user-profile",
  templateUrl: "./user-profile.page.html",
  styleUrls: ["./user-profile.page.scss"],
})
export class UserProfilePage implements OnInit {
  currentUser: User = new User();

  constructor(
    public authService: AuthService,
    private actRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    let id = this.actRoute.snapshot.paramMap.get("id");
    this.authService.getUserProfile(id).subscribe((res) => {
      this.currentUser = res.msg;
      //console.log(this.currentUser);
    });
  }

  logout() {
    this.authService.doLogout();
  }
}
